import java.util.*;

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node(String n, Node d, Node r) {
        name = n;
        firstChild = d;
        nextSibling = r;
    }

    public static Node parsePostfix(String s) {
        checkErrors(s);
        StringBuilder name = new StringBuilder();
        Stack<Node> stack = new Stack<>();
        stack.push(new Node("", null, null));

        Node current = stack.lastElement();
        for (int i = 0; i < s.length(); i++) {
            if ("(".equals(String.valueOf(s.charAt(i)))) {
                Node newNode = new Node("", null, null);
                stack.push(newNode);
                current.firstChild = newNode;
                current = newNode;
                continue;
            }
            if (")".equals(String.valueOf(s.charAt(i)))) {
                current.name = name.toString();
                name = new StringBuilder();
                Node last = stack.pop();
                if(stack.lastElement().nextSibling != null) {
                    current = stack.lastElement().nextSibling;
                }
                else {
                    current = stack.lastElement();}
                current.firstChild = last;
                continue;


            }
            if (",".equals(String.valueOf(s.charAt(i)))) {
                current.name = name.toString();
                name = new StringBuilder();
                Node newNode = new Node("", null, null);
                current.nextSibling = newNode;
                current = newNode;

            } else {
                name.append(s.charAt(i));
            }
        }
        stack.lastElement().name = name.toString();
        return stack.lastElement();
    }
    public static void checkErrors(String s){
        int brackets = 0;
        if(s.contains("\t")) {throw new RuntimeException("Illegal string  at string " + s);}
        for (int i = 0; i < s.length(); i++) {
            String character = String.valueOf(s.charAt(i));

            if (character.equals("(")){brackets++;}
            else if (character.equals(")")) {brackets--;}
            else if (character.equals(",") && brackets == 0) {
                throw new RuntimeException("Comma can not be at top of the tree! at string " + s);}
            if (character.equals(" ") && !String.valueOf(s.charAt(i - 1)).equals(",")){
                throw new RuntimeException("Empty space can not be in string!  at string"  + s);
            }
            else if (character.equals( ",") && String.valueOf(s.charAt(i - 1)).equals(",")){
                throw new RuntimeException("Two commas next to eachother at string"  + s);
            }
            else if(character.equals("(") && String.valueOf(s.charAt(i + 1)).equals(")")) {
                throw new RuntimeException("Empty subtree! at string " + s);
            }
            else if (s.contains(",") && !s.contains("(")) {
                throw new RuntimeException("Subtree is missing brackets  at string " + s);
            }
            else if (character.equals(")") && String.valueOf(s.charAt(i + 1)).equals(")")) {
                throw new RuntimeException("Double brackets at string " + s);
            }
            else if (character.equals("(") && String.valueOf(s.charAt(i + 1)).equals(",")) {
                throw new RuntimeException("Comma error  at string " + s);
            }

        }
    }
    public String leftParentheticRepresentation() {

        StringBuilder sb = new StringBuilder();
        sb.append(this.name);
        if (firstChild != null && nextSibling != null) {
            sb.append("(").append(firstChild.leftParentheticRepresentation()).append("),").append(nextSibling.leftParentheticRepresentation());
        } else if (firstChild != null) {
            sb.append("(").append(firstChild.leftParentheticRepresentation()).append(")");
        } else if (nextSibling != null) {
            sb.append(",").append(nextSibling.leftParentheticRepresentation());
        }
        return sb.toString();
    }

    public String xmlRepresentation(int counter){

        String newline = System.getProperty("line.separator");
        StringBuilder sb = new StringBuilder(newline);
        sb.append("\t".repeat(Math.max(0, counter - 1)));
        sb.append("<L").append(counter).append("> ").append(name).append(" ");
        if (firstChild != null) {
            sb.append(firstChild.xmlRepresentation(counter + 1)).append(newline);
            sb.append("\t".repeat(Math.max(0, counter - 1)));
        }
        sb.append("</L").append(counter).append("> "    );
        if (nextSibling != null) {
            sb.append(nextSibling.xmlRepresentation(counter));
        }
        return sb.toString();

    }

    public static void main(String[] param) {
        //Node x = new Node("A", new Node("B1", null, new Node("C", null, null)), null);
        String s = "(B,C,D,E,F)A";
        Node t = Node.parsePostfix(s);
        String v = t.xmlRepresentation(1);
        v = v.replace("\t", "");
        v = v.replace("\n", "");
        v = v.replace("\r", "");
        v = v.replace(" ", "");
        System.out.println(s + " ==> " + v); // (B1,C)A ==> A(B1,C)
    }
}

