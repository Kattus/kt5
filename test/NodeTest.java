
import static org.junit.Assert.*;

import org.junit.Test;
// import java.util.*;

/**
 * Testklass.
 *
 * @author Jaanus
 */
public class NodeTest {

    @Test(timeout = 1000)
    public void testParsePostfix() {
        String s = "(B1,C,D)A";
        Node t = Node.parsePostfix(s);
        String r = t.leftParentheticRepresentation();
        assertEquals("Tree: " + s, "A(B1,C,D)", r);
        s = "(((2,1)-,4)*,(69,3)/)+";
        t = Node.parsePostfix(s);
        r = t.leftParentheticRepresentation();
        assertEquals("Tree: " + s, "+(*(-(2,1),4),/(69,3))", r);
    }

    @Test(timeout = 1000)
    public void testParsePostfixAndLeftParentheticRepresentation1() {
        String s = "((((E)D)C)B)A";
        Node t = Node.parsePostfix(s);
        String r = t.leftParentheticRepresentation();
        assertEquals("Tree: " + s, "A(B(C(D(E))))", r);
        s = "((C,(E)D)B,F)A";
        t = Node.parsePostfix(s);
        r = t.leftParentheticRepresentation();
        assertEquals("Tree: " + s, "A(B(C,D(E)),F)", r);
    }

    @Test(timeout = 1000)
    public void testParsePostfixAndLeftParentheticRepresentation2() {
        String s = "(((512,1)-,4)*,(-6,3)/)+";
        Node t = Node.parsePostfix(s);
        String r = t.leftParentheticRepresentation();
        assertEquals("Tree: " + s, "+(*(-(512,1),4),/(-6,3))", r);
        s = "(B,C,D,E,F)A";
        t = Node.parsePostfix(s);
        r = t.leftParentheticRepresentation();
        assertEquals("Tree: " + s, "A(B,C,D,E,F)", r);
        s = "((1,(2)3,4)5)6";
        t = Node.parsePostfix(s);
        r = t.leftParentheticRepresentation();
        assertEquals("Tree: " + s, "6(5(1,3(2),4))", r);
    }

    @Test(timeout = 1000)
    public void testSingleRoot() {
        String s = "ABC";
        Node t = Node.parsePostfix(s);
        String r = t.leftParentheticRepresentation();
        assertEquals("Tree: " + s, "ABC", r);
        s = ".Y.";
        t = new Node(s, null, null);
        r = t.leftParentheticRepresentation();
        assertEquals("Single node" + s, s, r);
    }

    @Test(timeout = 1000)
    public void testXmlRepresentation1() {
        String s = "((C)B,(E,F)D,G)A";
        Node t = Node.parsePostfix(s);
        String r = t.xmlRepresentation(1);
        r = r.replace("\t", "");
        r = r.replace("\n", "");
        r = r.replace("\r", "");
        r = r.replace(" ", "");
        assertEquals("Tree: " + s, "<L1>A<L2>B<L3>C</L3></L2><L2>D<L3>E</L3><L3>F</L3></L2><L2>G</L2></L1>", r);
    }

    @Test(timeout = 1000)
    public void testXmlRepresentation2() {
        String s = "(B)A";
        Node t = Node.parsePostfix(s);
        String r = t.xmlRepresentation(1);
        r = r.replace("\t", "");
        r = r.replace("\n", "");
        r = r.replace("\r", "");
        r = r.replace(" ", "");
        assertEquals("Tree: " + s,
                "<L1>A<L2>B</L2></L1>", r);
    }

    @Test(timeout = 1000)
    public void testXmlRepresentation3() {
        String s = "(B,C,D,E,F)A";
        Node t = Node.parsePostfix(s);
        String r = t.xmlRepresentation(1);
        r = r.replace("\t", "");
        r = r.replace("\n", "");
        r = r.replace("\r", "");
        r = r.replace(" ", "");
        assertEquals("Tree: " + s,
                "<L1>A<L2>B</L2><L2>C</L2><L2>D</L2><L2>E</L2><L2>F</L2></L1>", r);
    }

    @Test(timeout = 1000)
    public void testXmlRepresentation4() {
        String s = "(((512,1)-,4)*,(-6,3)/)+";
        Node t = Node.parsePostfix(s);
        String r = t.xmlRepresentation(1);
        r = r.replace("\t", "");
        r = r.replace("\n", "");
        r = r.replace("\r", "");
        r = r.replace(" ", "");
        assertEquals("Tree: " + s, "<L1>+<L2>*<L3>-<L4>512</L4><L4>1</L4></L3><L3>4</L3></L2><L2>/<L3>-6</L3><L3>3</L3></L2></L1>", r);
    }


    @Test
    public void testSpaceInNodeName() {
        Node root = Node.parsePostfix("A B");
    }

    @Test
    public void testTwoCommas() {
        Node t = Node.parsePostfix("(B,,C)A");
    }

    @Test
    public void testEmptySubtree() {
        Node root = Node.parsePostfix("()A");
    }

    @Test
    public void testInputWithBracketsAndComma() {
        Node t = Node.parsePostfix("( , ) ");
    }

    @Test
    public void testInputWithoutBrackets() {
        Node t = Node.parsePostfix("A,B");
    }

    @Test
    public void testInputWithDoubleBrackets() {
        Node t = Node.parsePostfix("((C,D))A");
    }

    @Test
    public void testComma1() {
        Node root = Node.parsePostfix("(,B)A");
    }

    @Test
    public void testComma2() {
        Node root = Node.parsePostfix("(B)A,(D)C");
    }

    @Test
    public void testComma3() {
        Node root = Node.parsePostfix("(B,C)A,D");
    }

    @Test
    public void testTab1() {
        Node root = Node.parsePostfix("\t");
    }

    @Test
    public void testWeirdBrackets() {
        Node root = Node.parsePostfix(")A(");
    }
}
